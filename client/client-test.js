
const citations_exemple = [
    {
        quote: "Yolo.",
        character: "Aragorn",
        image: "https://un.site/image_invalide.jipégé",
        characterDirection: "Right",
        origin: "Le seigneur des anneaux",
        addedBy: "p1925107",
        scores: {
            "id3": {
                wins: 3,
                looses: 2
            },
            "id2": {
                wins: 3,
                looses: 2
            }
        },
        _id: "id1"
    },
    {
        quote: "Yo!",
        character: "Pitchou",
        image: "https://un.site/une_autre_image.jpg",
        characterDirection: "Left",
        origin: "Pitchou",
        addedBy: "p1925107",
        scores: {
            "id1": {
                wins: 2,
                looses: 3
            },
            "id3": {
                wins: 10,
                looses: 4
            }
        },
        _id: "id2"
    },
    {
        quote: "Bonjour!",
        character: "Tchoupi",
        image: "https://un.site/image.jpg",
        characterDirection: "Right",
        origin: "Tchoupi",
        addedBy: "p1925107",
        scores: {
            "id1": {
                wins: 2,
                looses: 3
            },
            "id2": {
                wins: 4,
                looses: 10
            }
        },
        _id: "id3"
    }
];

///////////////////////////////////////////////////////////////////////////////
// Suites de tests

