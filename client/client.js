/* ******************************************************************
 * Constantes de configuration
 */
const apiKey = "7be9f1c9-684d-4aad-be38-c2f9e70ecbf2";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* *******************************
 * Constantes et éléments utiles
 *********************************/

// Liste des noms des onglets qui servent de base aux ids correspondants
const tabs = ["tout", "duel", "ajouter"];

// Éléments clés de l'onglet duel
const cit1Text  = document.getElementById("citation1-text");
const cit2Text  = document.getElementById("citation2-text");
const cit1Img   = document.getElementById("citation1-img");
const cit2Img   = document.getElementById("citation2-img");
const cit1Vote  = document.getElementById("vote-gauche");
const cit2Vote  = document.getElementById("vote-droite");

// Éléments clés de l'onglet d'ajout de citation
const input_nom       = document.getElementById("input-nom");
const input_origine   = document.getElementById("input-origine");
const input_citation  = document.getElementById("input-citation");
const input_image     = document.getElementById("input-image");
const input_send      = document.getElementById("input-send");
const input_droite    = document.getElementById("input-is_right");
const input_gauche    = document.getElementById("input-is_left");
const citExempleText  = document.getElementById("citation0-text");
const citExempleImg   = document.getElementById("citation0-img");


let citationsData = {};

/**
 * Génère un duel de citation et met à jour la page
 */
function majCitationDuel() {
  fetchCitations().then((citations) => {
    let citation1 = citations[Math.floor(Math.random() * citations.length)];
    let citation2 = citations[Math.floor(Math.random() * citations.length)];
    while (citation2._id == citation1._id) {
      citation2 = citations[Math.floor(Math.random() * citations.length)];
    }

    cit1Text.innerHTML =
      `<p class="title">"${citation1.quote}"</p>` +
      `<p class="subtitle">${citation1.character} dans ${citation1.origin}</p>`;
    cit2Text.innerHTML =
      `<p class="title">"${citation2.quote}"</p>` +
      `<p class="subtitle">${citation2.character} dans ${citation2.origin}</p>`;

    if (citation1.characterDirection == "Left")
      cit1Img.innerHTML = `<img src="${citation1.image}" />`;
    else
      cit1Img.innerHTML = 
        `<img src="${citation1.image}" style="transform: scaleX(-1)" />`;

    if (citation2.characterDirection == "Right")
      cit2Img.innerHTML = `<img src="${citation2.image}" />`;
    else
      cit2Img.innerHTML = 
        `<img src="${citation2.image}" style="transform: scaleX(-1)" />`;

    cit1Vote.onclick = () => vote(citation1._id, citation2._id);
    cit2Vote.onclick = () => vote(citation2._id, citation1._id);
  });
}

/* *******************************
 * Onglet d'ajout de citation
 *********************************/

/**
 * Met à jour l'aspect du formulaire d'ajout de citation
 * en fonction de ce qui est entré
 */
function majCitationForm() {
  let valid = true;
  if (input_nom.value == "") valid = false;
  if (input_origine.value == "") valid = false;
  if (input_citation.value == "") valid = false;
  if (input_image.value.match(/\.(jpeg|jpg|png)$/) == null) valid = false;

  // Si aucun problème n'a été détécté, autoriser à valider
  if (valid) input_send.disabled = false;
  // Sinon, empêcher la validation
  else input_send.disabled = true;
}

/**
 * Réinitialise les champs du formulaire d'ajout de citation
 */
function effacerCitationForm() {
  input_nom.value = "";
  input_origine.value = "";
  input_citation.value = "";
  input_image.value = "";
  majCitationExemple();
}


/**
 * Envoie la citation au serveur et informe de la réussite ou de l'échec
 */
function envoyerCitation() {
  const direction = (input_droite.checked ? "Right" : "Left");

  let result = fetch(serverUrl + "/citations", {
    method: "POST",
    headers: { "x-api-key": apiKey, "Content-Type": "application/json" },
    body: JSON.stringify({
      quote: input_citation.value,
      character: input_nom.value,
      image: input_image.value,
      characterDirection: direction,
      origin: input_origine.value
    }),
  });
  effacerCitationForm();
}

/**
 * Met à jour la visualisation de citation en fonction
 * du contenu du formulaire
 */
function majCitationExemple() {
  citExempleText.innerHTML =
    `<p class="title">"${input_citation.value}"</p>` +
    `<p class="subtitle">${input_nom.value} dans ${input_origine.value}</p>`;
  
  if (input_droite.checked)
    citExempleImg.innerHTML = 
    `<img src="${input_image.value}" style="transform: scaleX(-1)"/>`;
  else
    citExempleImg.innerHTML = `<img src="${input_image.value}" />`;
  majCitationForm();
};

/**
 * Assigne les callbacks aux éléments du formulaire d'ajout de citation
 */
function initFormCallbacks() {
  input_nom.oninput      = majCitationExemple;
  input_origine.oninput  = majCitationExemple;
  input_citation.oninput = majCitationExemple;
  input_image.oninput    = majCitationExemple;
  input_gauche.onclick   = majCitationExemple;
  input_droite.onclick   = majCitationExemple;
  input_send.onclick     = envoyerCitation;
}



/* *******************************
 * Onglet "Toutes les citations"
 *********************************/

/**
 * Stocke les citations indexées par id dans citationsData
 * @param {json} citations La liste de citations renvoyée par fetchCitations()
 */
function storeCitationsData(citations) {
  citationsData = citations.reduce((acc, citation, index) => {
    acc[citation._id] = citation;
    return acc;
  },
  {});
}

/**
 * Génère le code d'un \<tr> à placer dans la liste des citations
 * @param {citation} citation Une citation telle que reçue de l'API
 * @param {any} value La valeur à afficher en tête de ligne
 * @returns Le code HTML d'un \<tr> décrivant la citation
 */
function genererAffichageCitation(citation, value) {
  return `<tr class="citation" id="citation-${citation._id}">` +
  `<th>${value}</th><td>${citation.character}</td><td>${
    citation.quote
  }</td>` +
  `</tr>`;
}

/**
 * Génère et renvoie les détails de la citation donnée
 * @param {citation} citation La citation dont on veut afficher les détails
 * @returns Le code html d'un \<tr> caché de 3 colonnes
 */
function genererDetailsCitation(citation) {
  return `<tr class="citation_details" style="display:none">` +
  `<th>Details</th><td colspan="2"><table>` +
  `<tr><td>` +
  `<button class="button" onclick="afficherModalScores('${citation._id}')">` +
  `Scores</button></td></tr>` +
  `<tr><th>quote</th><td>${citation.quote}</td></tr>` +
  `<tr><th>character</th><td>${citation.character}</td></tr>` +
  `<tr><th>origin</th><td>${citation.origin}</td></tr>` +
  `<tr><th>image link</th><td>${citation.image}</td></tr>` +
  `<tr><th>character direction</th>` +
  `<td>${citation.characterDirection}</td></tr>` +
  `<tr><th>added by</th><td>${citation.addedBy}</td></tr>` +
  `</table></td></tr>`
}

/**
 * Récupère la liste des citations sur le serveur
 * et met à jour celle affichée sur la page web
 */
function majCitationsTout() {
  fetchCitations().then((citations) => {
    storeCitationsData(citations);
    const tableBody = document.getElementById("all-citations");
    citationsData.innerHTML = JSON.stringify(citations);
    tableBody.innerHTML = citations.reduce(
      (acc, citation, index) =>
        acc +
        genererAffichageCitation(citation, index+1) +
        genererDetailsCitation(citation),
      ""
    ); // Pour chaque citation, on ajoute une ligne cachée avec ses détails
    Array.from(document.getElementsByClassName("citation")).forEach(
      (citation) => {
        citation.nextSibling.style.display = "none";
        citation.onclick = () => expand_citation(citation.nextSibling);
      }
    );
  });
}

/**
 * Affiche les détails de la citation cliquée
 *
 * @param {tr} citation_details La ligne de tableau contenant les détails de la citation
 */
function expand_citation(citation_details) {
  citation_details.style.display = "table-row";
  citation_details.previousSibling.onclick = () =>
    retract_citation(citation_details);
}

/**
 * Masque les détails de la citation cliquée
 *
 * @param {tr} citation_details La ligne de tableau contenant les détails de la citation
 */
function retract_citation(citation_details) {
  citation_details.style.display = "none";
  citation_details.previousSibling.onclick = () =>
    expand_citation(citation_details);
}

/**
 * Génère un nouveau duel et envoie le vote au serveur
 *
 * @param {string} winner L'id de la citation gagnante
 * @param {string} loser L'id de la citation perdante
 * @returns Les 2 citations mises à jour
 */
function vote(winner, loser) {
  majCitationDuel();
  return fetch(serverUrl + "/citations/duels", {
    method: "POST",
    headers: { "x-api-key": apiKey, "Content-Type": "application/json" },
    body: JSON.stringify({
      winner: winner,
      looser: loser,
    }),
  });
}

/**
 * Ferme le modal d'affichage des scores
 */
function fermerModalScores() {
  document.getElementById("details-score-modal").classList.remove("is-active");
}

/**
 * Affiche le modal de scores de la citation choisie
 * @param {id} citation_id L'id de la citation concernée
 */
function afficherModalScores(citation_id) {
  const scores = citationsData[citation_id].scores;
  const modal = document.getElementById("details-score-modal");
  const titre = document.getElementById("details-score-citation");
  const table = document.getElementById("details-score-table");

  titre.innerHTML = citationsData[citation_id].quote;
  if (scores != undefined)
    table.innerHTML = Object.keys(scores).reduce( (acc, adversary_id, index) =>
      acc +
      `<tr><td>${citationsData[adversary_id].quote}</td>` +
      `<td>${scores[adversary_id].wins}</td>` +
      `<td>${scores[adversary_id].looses}</td></tr>`
    ,
    "");
  else
    table.innerHTML = '<p class="title">Cette citation n\'a pas eu' +
                      ' de duel pour le moment</p>';

  modal.classList.add("is-active");
}

/**
 * Initialise les callbacks des éléments du modal d'affichage du score
 */
function initModalScoresCallbacks() {
  document.getElementById("bg-score-modal").onclick  = fermerModalScores;
}

/* *******************************
 * Navigation
 *********************************/

/**
 * Récupère la liste des citations sur le serveur en json
 * (voir https://lifap5.univ-lyon1.fr/api-docs/#/citations/get_citations)
 */
async function fetchCitations() {
  console.log("CALL fetchCitations");
  return fetch(serverUrl + "/citations", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      return jsonData;
    });
}

/**
 * Affiche/masque les divs
 * selon le tab indiqué dans l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majTab(etatCourant) {
  console.log("CALL majTab");
  tabs.forEach((tab) => {
    const dTab = document.getElementById(`div-${tab}`);
    const tTab = document.getElementById(`tab-${tab}`);
    if (tab === etatCourant.tab) {
      dTab.style.display = "flex";
      tTab.classList.add("is-active");
    } else {
      dTab.style.display = "none";
      tTab.classList.remove("is-active");
    }
  });
}

/**
 * Mets au besoin à jour l'état courant lors d'un click sur un tab.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} tab le nom du tab qui a été cliqué
 * @param {Etat} etatCourant l'état courant
 */
function clickTab(tab, etatCourant) {
  console.log(`CALL clickTab(${tab},...)`);
  if (etatCourant.tab !== tab) {
    etatCourant.tab = tab;
    majPage(etatCourant);
  }
}

/**
 * Enregistre les fonctions à utiliser lorsque l'on clique
 * sur un des tabs.
 *
 * @param {Etat} etatCourant l'état courant
 */
function registerTabClick(etatCourant) {
  console.log("CALL registerTabClick");
  document.getElementById("tab-duel").onclick = () =>
    clickTab("duel", etatCourant);
  document.getElementById("tab-tout").onclick = () =>
    clickTab("tout", etatCourant);
  document.getElementById("tab-ajouter").onclick = () =>
    clickTab("ajouter", etatCourant);
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami() {
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans
 * la modale d'affichage de l'utilisateur.
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin() {
  return fetchWhoami().then((data) => {
    const elt = document.getElementById("elt-affichage-login");
    const ok = data.err === undefined;
    if (!ok) {
      elt.innerHTML = `<span class="is-error">${data.err}</span>`;
    } else {
      elt.innerHTML = `Bonjour ${data.login}.`;
    }
    return ok;
  });
}

/**
 * Affiche ou masque la fenêtre modale de login en fonction de l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majModalLogin(etatCourant) {
  const modalClasses = document.getElementById("mdl-login").classList;
  if (etatCourant.loginModal) {
    modalClasses.add("is-active");
    lanceWhoamiEtInsereLogin();
  } else {
    modalClasses.remove("is-active");
  }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickFermeModalLogin(etatCourant) {
  etatCourant.loginModal = false;
  majPage(etatCourant);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickOuvreModalLogin(etatCourant) {
  etatCourant.loginModal = true;
  majPage(etatCourant);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant l'utilisateur.
 * @param {Etat} etatCourant
 */
function registerLoginModalClick(etatCourant) {
  document.getElementById("btn-close-login-modal1").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-close-login-modal2").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-open-login-modal").onclick = () =>
    clickOuvreModalLogin(etatCourant);
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  majTab(etatCourant);
  if (etatCourant.tab == "tout")
    majCitationsTout();
  majModalLogin(etatCourant);
  registerTabClick(etatCourant);
  registerLoginModalClick(etatCourant);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientCitations() {
  console.log("CALL initClientCitations");
  const etatInitial = {
    tab: "duel",
    loginModal: false,
  };
  majPage(etatInitial);
  majCitationsTout();
  majCitationDuel();
  initFormCallbacks();
  initModalScoresCallbacks();
  majCitationExemple();
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  if (document.getElementById("mocha") == null) {
    console.log("Exécution du code après chargement de la page");
    initClientCitations();
  }
});
